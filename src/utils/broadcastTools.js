/**
 * 跨浏览器标签页通信工具
 *
 */

// 用户退出登录渠道
export const CHANNEL_LOGOUT = 'logout'

export const createBroad = (channel, onmessage) => {
    const broad = new BroadcastChannel(channel);
    if (onmessage) {
        broad.onmessage = onmessage
    }
    return broad
}

export const postBroadMessage = (channel, message) => {
    const broad = new BroadcastChannel(channel);
    broad.postMessage(message)
}