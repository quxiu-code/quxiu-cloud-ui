import request from '@/utils/request'

let baseUrl = '/workFlow/'

// 设置审批数据
export function getWorkFlowData(data) {
  return request({
    baseURL: "/",
    url: `${baseUrl}data.json`,
    method: 'post',
    data: data
  })
}
