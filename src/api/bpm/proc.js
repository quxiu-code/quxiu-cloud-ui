import request from '@/utils/request'

// 查询流程定义列表
export function listProc(query) {
  return request({
    url: '/system/bpm/proc/list',
    method: 'get',
    params: query
  })
}

// 查询流程定义详细
export function getProc(procId) {
  return request({
    url: '/system/bpm/proc/' + procId,
    method: 'get'
  })
}

// 新增流程定义
export function addProc(data) {
  return request({
    url: '/system/bpm/proc',
    method: 'post',
    data: data
  })
}

// 修改流程定义
export function updateProc(data) {
  return request({
    url: '/system/bpm/proc',
    method: 'put',
    data: data
  })
}

// 删除流程定义
export function delProc(procId) {
  return request({
    url: '/system/bpm/proc/' + procId,
    method: 'delete'
  })
}

// 按分组获取流程定义
export function getProcForGroup() {
  return request({
    url: '/system/bpm/proc/getProcForGroup',
    method: 'post'
  })
}