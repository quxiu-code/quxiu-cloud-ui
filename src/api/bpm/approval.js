import request from '@/utils/request'

// 审批
export function approval(data) {
  return request({
    url: '/system/bpm/approval/approval',
    method: 'post',
    data: data
  })
}

// 撤消
export function revoke(data) {
  return request({
    url: '/system/bpm/approval/revoke',
    method: 'post',
    data: data
  })
}

// 加签
export function addSign(data) {
  return request({
    url: '/system/bpm/approval/addSign',
    method: 'post',
    data: data
  })
}

// 转交
export function forward(data) {
  return request({
    url: '/system/bpm/approval/forward',
    method: 'post',
    data: data
  })
}
// 获取回退节点列表
export function getRollbackNodes(data) {
  return request({
    url: '/system/bpm/approval/getRollbackNodes',
    method: 'post',
    data: data
  })
}
// 回退
export function rollback(data) {
  return request({
    url: '/system/bpm/approval/rollback',
    method: 'post',
    data: data
  })
}


// 查询流程交接列表
export function handoverList(query) {
  return request({
    url: '/system/bpm/approval/handoverList',
    method: 'get',
    params: query
  })
}

// 流程交接
export function handover(data) {
  return request({
    url: '/system/bpm/approval/handover',
    method: 'post',
    data: data
  })
}