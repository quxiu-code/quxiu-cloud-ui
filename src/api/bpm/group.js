import request from '@/utils/request'

// 查询流程分组列表
export function listGroup(query) {
  return request({
    url: '/system/bpm/group/list',
    method: 'get',
    params: query
  })
}

// 查询流程分组详细
export function getGroup(groupId) {
  return request({
    url: '/system/bpm/group/' + groupId,
    method: 'get'
  })
}

// 新增流程分组
export function addGroup(data) {
  return request({
    url: '/system/bpm/group',
    method: 'post',
    data: data
  })
}

// 修改流程分组
export function updateGroup(data) {
  return request({
    url: '/system/bpm/group',
    method: 'put',
    data: data
  })
}

// 删除流程分组
export function delGroup(groupId) {
  return request({
    url: '/system/bpm/group/' + groupId,
    method: 'delete'
  })
}
