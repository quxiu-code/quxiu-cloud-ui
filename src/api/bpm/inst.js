import request from '@/utils/request'

// 查询流程实例列表
export function myApproveList(query) {
  return request({
    url: '/system/bpm/inst/myApproveList',
    method: 'get',
    params: query
  })
}

// 查询流程实例详细
export function getInst(procInstId) {
  return request({
    url: '/system/bpm/inst/' + procInstId,
    method: 'get'
  })
}

// 新增流程实例
export function addInst(data) {
  return request({
    url: '/system/bpm/inst',
    method: 'post',
    data: data
  })
}

// 修改流程实例
export function updateInst(data) {
  return request({
    url: '/system/bpm/inst',
    method: 'put',
    data: data
  })
}

// 删除流程实例
export function delInst(procInstId) {
  return request({
    url: '/system/bpm/inst/' + procInstId,
    method: 'delete'
  })
}

// 计算审批节点
export function calculateNode(data) {
  return request({
    url: '/system/bpm/inst/calculateNode',
    method: 'post',
    data: data
  })
}

// 获取审批记录
export function getApprovalRecord(procInstId) {
  return request({
    url: '/system/bpm/inst/getApprovalRecord',
    method: 'get',
    params: {
      procInstId: procInstId
    }
  })
}

// 审批看版，获取当前登录人【我发起的、待处理、已处理、我收到的】的审批记录数量
export function getApprovalBoard() {
  return request({
    url: '/system/bpm/inst/statistics/getApprovalBoard',
    method: 'get'
  })
}