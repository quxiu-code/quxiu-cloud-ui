import request from '@/utils/request'

// 查询数据实体列表
export function listEntity(query) {
  return request({
    url: '/system/magic/entity/list',
    method: 'get',
    params: query
  })
}

// 查询数据实体详细
export function getEntity(entityId) {
  return request({
    url: '/system/magic/entity/' + entityId,
    method: 'get'
  })
}

// 新增数据实体
export function addEntity(data) {
  return request({
    url: '/system/magic/entity',
    method: 'post',
    data: data
  })
}

// 修改数据实体
export function updateEntity(data) {
  return request({
    url: '/system/magic/entity',
    method: 'put',
    data: data
  })
}

// 删除数据实体
export function delEntity(entityId) {
  return request({
    url: '/system/magic/entity/' + entityId,
    method: 'delete'
  })
}
