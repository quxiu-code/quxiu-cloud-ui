import request from '@/utils/request'

// 查询应用列表
export function listApp(query) {
  return request({
    url: '/system/app/list',
    method: 'get',
    params: query
  })
}

// 查询应用详细
export function getApp(appId) {
  return request({
    url: '/system/app/' + appId,
    method: 'get'
  })
}

// 新增应用
export function addApp(data) {
  return request({
    url: '/system/app',
    method: 'post',
    data: data
  })
}

// 修改应用
export function updateApp(data) {
  return request({
    url: '/system/app',
    method: 'put',
    data: data
  })
}

// 删除应用
export function delApp(appId) {
  return request({
    url: '/system/app/' + appId,
    method: 'delete'
  })
}

// 全量推送用户数据
export function pushUserData(appId) {
  return request({
    url: '/system/app/pushUserData',
    method: 'post',
    data: appId
  })
}

// 全量推送部门数据
export function pushDeptData(appId) {
  return request({
    url: '/system/app/pushDeptData',
    method: 'post',
    data: appId
  })
}

// 生成应用密钥对
export function generateKeyPair() {
  return request({
    url: '/system/app/generateKeyPair',
    method: 'get'
  })
}