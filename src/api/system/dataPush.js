import request from '@/utils/request'

// 查询数据推送记录列表
export function listDataPush(query) {
  return request({
    url: '/system/dataPush/list',
    method: 'get',
    params: query
  })
}

// 查询数据推送记录详细
export function getDataPush(recordId) {
  return request({
    url: '/system/dataPush/' + recordId,
    method: 'get'
  })
}
