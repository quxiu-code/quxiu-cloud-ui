const WorkFlowStore = defineStore(
  'workFlow',
  {
    state: () => ({
      tableId: '',
      isTried: false,
      promoterDrawer: false,
      flowPermission1: {},
      approverDrawer: false,
      approverConfig1: {},
      copyerDrawer: false,
      copyerConfig1: {},
      conditionDrawer: false,
      conditionsConfig1: {
        conditionNodes: [],
      },
    }),
    actions: {

    }
  })

export default WorkFlowStore
