export const operatorMap = new Map()

// 等于
export const EQ = {
    name: 'eq',
    label: '等于',
    symbol: '='
}
operatorMap.set(EQ.name, EQ)

// 不等于
export const NE = {
    name: 'ne',
    label: '不等于',
    symbol: '!='
}
operatorMap.set(NE.name, NE)

// 大于
export const GT = {
    name: 'gt',
    label: '大于',
    symbol: '>'
}
operatorMap.set(GT.name, GT)
// 大于等于
export const GE = {
    name: 'ge',
    label: '大于等于',
    symbol: '>='
}
operatorMap.set(GE.name, GE)

// 小于
export const LT = {
    name: 'lt',
    label: '小于',
    symbol: '<'
}
operatorMap.set(LT.name, LT)

// 小于等于
export const LE = {
    name: 'le',
    label: '小于等于',
    symbol: '<='
}
operatorMap.set(LE.name, LE)

// 介于
export const BT = {
    name: 'bt',
    label: '介于',
    symbol: '介于'
}
operatorMap.set(BT.name, BT)

// 包含任意
export const IN = {
    name: 'in',
    label: '包含任意',
    symbol: '包含任意'
}
operatorMap.set(IN.name, IN)

// 完全包含
export const INE = {
    name: 'ine',
    label: '完全包含',
    symbol: '完全包含'
}
operatorMap.set(INE.name, INE)


