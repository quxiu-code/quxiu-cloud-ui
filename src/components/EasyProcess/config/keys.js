
// 流程控制器KEY
export const KEY_PROCESS_CTRL = 'processCtrl'

// 节点验证器KEY
export const KEY_VALIDATOR = 'validator'

// 流程数据KEY
export const KEY_PROCESS_DATA = 'processData'