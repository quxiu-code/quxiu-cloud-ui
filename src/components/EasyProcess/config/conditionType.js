
// 数值
export const NUMBER = 'number'

// 复选框
export const CHECKBOX = 'checkbox'

// 下拉框
export const SELECT = 'select'

// 单选按钮
export const RADIO = 'radio'

export const conditionTypes = [NUMBER, CHECKBOX, SELECT, RADIO]