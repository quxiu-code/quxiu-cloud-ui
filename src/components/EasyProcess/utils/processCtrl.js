import {START, APPROVER, NOTIFY, CONDITION} from "../config/nodeType"

/**
 * 流程控制器
 * @returns {{}}
 */
export const createProcessCtrl = () => {
    let processCtrl = {};
    processCtrl.nodes = new Map()
    processCtrl.formFields = []

    // 添加节点
    processCtrl.addNode = (nodeId, nodeConfig) => {
        processCtrl.nodes.set(nodeId, nodeConfig)
        processCtrl.updateNodePermissions()
    }

    // 获取节点
    processCtrl.getNode = (nodeId) => {
        return processCtrl.nodes.get(nodeId)
    }

    // 移除节点
    processCtrl.removeNode = (nodeId) => {
        processCtrl.nodes.delete(nodeId)
    }

    // 设置表单字段
    processCtrl.setFormFields = (formFields) => {
        processCtrl.formFields = formFields || []
    }

    // 获取表单字段
    processCtrl.getFormFields = () => {
        return processCtrl.formFields
    }

    // 获取表单字段
    processCtrl.getFormFieldsMap = () => {
        let map = new Map;
        processCtrl.formFields.forEach(item => {
            map.set(item.name, item)
        })
        return map
    }

    // 更新节点权限
    processCtrl.updateNodePermissions = () => {
        let keys = Object.keys(processCtrl.nodes)

        processCtrl.nodes.forEach((node, key) => {
            let oldPermissions = node.config.permissions
            if (!oldPermissions) {
                oldPermissions = []
            }
            let rule = 2
            if (START === node.nodeType) {
                rule = 1
            }
            let tmpPermissions = {}
            processCtrl.formFields.forEach(field => {
                let old = oldPermissions[field.name]
                // if (old) {
                //     tmpPermissions[field.name] = {...old}
                // } else {
                //
                // }

                tmpPermissions[field.name] = {
                    label: field.label,
                    name: field.name,
                    rule: old ? old.rule : rule,
                }
            })
            node.config.permissions = tmpPermissions
        })
    }

    /**
     * 判断是否为已使用条件字段
     * @param fieldName
     * @returns {boolean}
     */
    processCtrl.isUsedConditionField = (fieldName) => {
        if (fieldName == null || fieldName === '') {
            return false
        }

        let exist = false
        processCtrl.nodes.forEach((node, key) => {

            if (node != null && node.nodeType === CONDITION && node.config != null && node.config.conditions != null) {
                let conditions = node.config.conditions
                for (let j=0; j<conditions.length; j++) {
                    let group = conditions[j]
                    if (group == null) {
                        continue
                    }
                    for (let k=0; k<group.length; k++) {
                        let condition = group[k]
                        if (condition != null) {
                            if (fieldName === condition.name) {
                                exist = true
                                break
                            }

                        }
                    }
                    if (exist) {
                        break
                    }
                }
            }
        })
        return exist
    }

    return processCtrl
}
